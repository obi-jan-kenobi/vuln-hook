use std::io::{self, Read};
use serde::Deserialize;
use exitcode;

#[derive(Deserialize)]
struct Vulnerabilities {
    high: u64,
    critical: u64,
}

#[derive(Deserialize)]
struct Metadata {
    vulnerabilities: Vulnerabilities
}


#[derive(Deserialize)]
struct Audit {
    metadata: Metadata
}


fn main() -> io::Result<()> {
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;
    let v: Audit = serde_json::from_str(&buffer)?;
    if v.metadata.vulnerabilities.high > 0 {
        eprintln!("high vulnerability found. consider updating");
    }
    if v.metadata.vulnerabilities.critical > 0 {
        eprintln!("critical vulnerability found!");
        std::process::exit(exitcode::DATAERR);
    }
    Ok(())
}
